#!/usr/bin/python

import ConfigParser
import rrdtool

# Parse config file
config = ConfigParser.ConfigParser()
config.read("config.ini")
rrdfile = config.get("Paths", "rrdfile")
sensorfiles = config._sections["Sensors"]
# TODO: Find better solution for removing "__name__" key from the result of config._sections
sensorfiles.popitem(last=False)
print sensorfiles

# Define the function for reading the temperature
def read_temp(file):
  tfile = open(file)
  text = tfile.read()
  tfile.close()
  tlines = text.split("\n")
  if tlines[0].find("YES") > 0:
    temp = float((tlines[1].split(" ")[9])[2:])
    temp /= 1000
    return temp

def read_all():
  template = ""
  update = "N:"
  for sensor in sensorfiles:
    print sensor
    template += "{0}:".format(sensor)
    temp = read_temp(sensorfiles[sensor])
    update += "{0}:".format(temp)
# Why is the last item in the list excempt from the update?
  update = update[:-1]
  template = template[:-1]
  rrdtool.update(rrdfile, "--template", template, update)

read_all()
