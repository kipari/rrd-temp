#!/bin/bash

RRDFILE=$(awk -F "=" '/rrdfile/ {print $2}' config.ini)
IMGPATH=$(awk -F "=" '/imgpath/ {print $2}' config.ini)
IMGHEIGHT=$(awk -F "=" '/imgheight/ {print $2}' config.ini)
IMGWIDTH=$(awk -F "=" '/imgwidth/ {print $2}' config.ini)
COLOR1=$(awk -F "=" '/color1/ {print $2}' config.ini)
COLOR2=$(awk -F "=" '/color2/ {print $2}' config.ini)
COLOR3=$(awk -F "=" '/color3/ {print $2}' config.ini)
NAME1=$(awk -F "=" '/name1/ {print $2}' config.ini)
NAME2=$(awk -F "=" '/name2/ {print $2}' config.ini)
NAME3=$(awk -F "=" '/name3/ {print $2}' config.ini)
COLORCANVAS=$(awk -F "=" '/colorcanvas/ {print $2}' config.ini)
COLORFONT=$(awk -F "=" '/colorfont/ {print $2}' config.ini)
COLORBACK=$(awk -F "=" '/colorback/ {print $2}' config.ini)

#1hour
rrdtool graph $IMGPATH/1hour.png --start -1h \
--full-size-mode \
--width=$IMGWIDTH --height=$IMGHEIGHT \
--slope-mode \
-c CANVAS$COLORCANVAS -c FONT$COLORFONT -c BACK$COLORBACK \
--vertical-label="Temperature (°C)" \
DEF:temp1=$RRDFILE:a:AVERAGE \
DEF:temp2=$RRDFILE:b:AVERAGE \
DEF:temp3=$RRDFILE:c:AVERAGE \
LINE2:temp1$COLOR1:$NAME1 \
LINE2:temp2$COLOR2:$NAME2 \
LINE2:temp3$COLOR3:$NAME3

#6hour
rrdtool graph $IMGPATH/6hour.png --start -6h \
--full-size-mode \
--width=$IMGWIDTH --height=$IMGHEIGHT \
--slope-mode \
-c CANVAS$COLORCANVAS -c FONT$COLORFONT -c BACK$COLORBACK \
--vertical-label="Temperature (°C)" \
DEF:temp1=$RRDFILE:a:AVERAGE \
DEF:temp2=$RRDFILE:b:AVERAGE \
DEF:temp3=$RRDFILE:c:AVERAGE \
LINE2:temp1$COLOR1:$NAME1 \
LINE2:temp2$COLOR2:$NAME2 \
LINE2:temp3$COLOR3:$NAME3

#1day
rrdtool graph $IMGPATH/1day.png --start -1d \
--full-size-mode \
--width=$IMGWIDTH --height=$IMGHEIGHT \
--slope-mode \
-c CANVAS$COLORCANVAS -c FONT$COLORFONT -c BACK$COLORBACK \
--vertical-label="Temperature (°C)" \
DEF:temp1=$RRDFILE:a:AVERAGE \
DEF:temp2=$RRDFILE:b:AVERAGE \
DEF:temp3=$RRDFILE:c:AVERAGE \
LINE2:temp1$COLOR1:$NAME1 \
LINE2:temp2$COLOR2:$NAME2 \
LINE2:temp3$COLOR3:$NAME3

#1week
rrdtool graph $IMGPATH/1week.png --start -1w \
--full-size-mode \
--width=$IMGWIDTH --height=$IMGHEIGHT \
--slope-mode \
-c CANVAS$COLORCANVAS -c FONT$COLORFONT -c BACK$COLORBACK \
--vertical-label="Temperature (°C)" \
DEF:temp1=$RRDFILE:a:AVERAGE \
DEF:temp2=$RRDFILE:b:AVERAGE \
DEF:temp3=$RRDFILE:c:AVERAGE \
LINE2:temp1$COLOR1:$NAME1 \
LINE2:temp2$COLOR2:$NAME2 \
LINE2:temp3$COLOR3:$NAME3
